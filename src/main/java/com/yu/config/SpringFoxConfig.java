package com.yu.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@EnableSwagger2
@Configuration
public class SpringFoxConfig {

    @Bean
    public Docket getDocket() {
        return new Docket(DocumentationType.SWAGGER_2).
                select().
                apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .apiInfo(getInfo());
    }

    /**
     *  对API文档进行全局配置
     */
    private ApiInfo getInfo() {
        ApiInfo api = new ApiInfoBuilder()
                .title("YuZhu API")
                .version("1.0")
                .termsOfServiceUrl("http://www.zhiyuan.com")
                .license("***科技有限公司版权")
                .description("***项目的帮助文档")
                .contact(
                        new Contact(
                                "qiangGe",
                                "http://www.zhiyuan.com",
                                "admin@zhiyuan.com"))
                .licenseUrl("http://www.zhiyuan.com")
                .extensions(Collections.emptyList())
                .build();
        return api;
    }
}
