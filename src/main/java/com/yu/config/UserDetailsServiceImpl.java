package com.yu.config;


import com.yu.mapper.UserMapper;
import com.yu.pojo.Users;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Resource
    UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        Users myuser = userMapper.find_user(s);
        List<GrantedAuthority> authorities= new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority(s));
        String pwd = "";
        if(myuser!=null){
            pwd = myuser.getPassword();
        }
        System.out.println("---------myuser.getPassword()-------------->"+myuser.getPassword());
        User user  = new User(s,pwd,true,true,true,true,authorities);
        return user;
    }
}
