package com.yu.mapper;

import com.yu.pojo.Commodity;
import com.yu.pojo.CommodityType;
import com.yu.pojo.PicCommodity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@Mapper
public interface CommodityMapper {

    //1.查看所有商品信息
    List<Commodity> getCommdityList();

    //2.根据id查商品
    Commodity getCommdityById(Long id);

    //3.增加商品
    Integer addCommodity(Commodity commodity);

    //4.根据id删除商品
    Integer deleteCommodity(Long id);

    //5.修改商品信息
    Long updateCommodity(Commodity commodity);

    //6.updateTest
//    Integer updateTest(@Param("commodityId") int i, @Param("id") int j);
    Integer updateTest(  Integer id);

    //7.分页获得总数
//    @Select("select count(1) from commodity where name like concat('%',#{name},'%')")
    Integer getSum(Map<String,Object> map);

    //8.分页
    List<Commodity> commodityList(Map<String,Object> map);

    //9.图片路径赋值picpath

    Integer addPicCommodityList(PicCommodity picCommodity);

    //10.getAllCommodityType()
    @Select("select * from commoditytype")
    List<CommodityType> getAllCommodityType();
}
