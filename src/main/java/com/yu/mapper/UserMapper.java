package com.yu.mapper;

import com.yu.pojo.Users;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.Map;

@Mapper
public interface UserMapper {

//    int add_user(Map map);
//    String is_user_openid_repeat(String openid);
//    int update_user(Map map);
//    int get_uid(Map map);
    Users find_user(String nickName);

//    @Select("select token from users where openId=#{openId}")
    String is_user_openid_repeat(String openId);

    Integer update_user_token(Map map);

    Integer add_user(Map map);

    Integer update_user(Map map);
}
