package com.yu.mapper;

import com.yu.pojo.Merchant;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MerchantMapper {

    List<Merchant> getAll();
}
