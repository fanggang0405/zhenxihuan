package com.yu.service;

import com.yu.pojo.Commodity;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

public interface CommodityService {

    // 1.查看所有商品信息
    Map<String, Object> getCommdityList();

    //2.根据id查商品
    Map<String, Object> getCommdityById(Long id);

    //3.增加商品
    Map<String, Object> addCommodity(Commodity commodity);

    //4.根据id删除商品
    Map<String, Object> deleteCommodity(Long id);

    //5.修改商品信息
    Map<String, Object> updateCommodity(Commodity commodity);

    //6.updateTest
    Map<String, Object> updateTest();

    //.分页
    Map<String, Object> commodityList(Map<String,Object> map);
}
