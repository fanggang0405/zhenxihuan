package com.yu.service.impl;

import com.yu.mapper.CommodityMapper;
import com.yu.pojo.Commodity;
import com.yu.pojo.PicCommodity;
import com.yu.service.CommodityService;
import com.yu.util.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class CommodityServiceImpl implements CommodityService {

    @Resource
    private CommodityMapper commodityMapper;

    // 1.查看所有商品信息
    @Override
    public Map<String, Object> getCommdityList() {

        Map<String, Object> map = new ConcurrentHashMap<>();
        List<Commodity> list = commodityMapper.getCommdityList();
        map.put("list", list);
        return map;
    }

    //2.根据id查商品
    @Override
    public Map<String, Object> getCommdityById(Long id) {
        Map<String, Object> map = new ConcurrentHashMap<>();
        if (id <= 0) {
            map.put("commodity", null);
            return map;
        }
        Commodity commodity = commodityMapper.getCommdityById(id);
        map.put("commodity", commodity);
        return map;
    }


    //3.增加商品
    @Override
    @Transactional(propagation = Propagation.REQUIRED, timeout = -1, rollbackFor = Exception.class)
    public Map<String, Object> addCommodity(Commodity commodity) {
        Map<String, Object> map = new ConcurrentHashMap<>();
        commodity.setCreateDate(new Date());
        commodity.setPicpath(commodity.getPicCommodityList().get(0).getPicpath());
        commodity.setMerchantId(commodity.getMerchant().getId());
        commodity.setType(commodity.getCommodityType().getId());
        commodity.setStatus(1L);
        commodity.setPeriod(3L);
        try {

            Long num = commodityMapper.addCommodity(commodity) + 0L;
            map.put("num", num);
            List<Integer> list = new ArrayList<>();
            if(num>0) {
                for (PicCommodity picCommodity : commodity.getPicCommodityList()) {
                    picCommodity.setCommodityId(commodity.getId().intValue());
                    Integer numPic = commodityMapper.addPicCommodityList(picCommodity);
                    if(numPic>0){
                        list.add(numPic);
                    }
                }
                map.put("list",list);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        map.put("commodity", commodity);
        return map;
    }


    //4.根据id删除商品
    @Override
    @Transactional(propagation = Propagation.REQUIRED, timeout = -1, rollbackFor = Exception.class)
    public Map<String, Object> deleteCommodity(Long id) {
        Map<String, Object> map = new ConcurrentHashMap<>();
        if (id <= 0) {
            map.put("num", 0);
            return map;
        }
        Long num = commodityMapper.deleteCommodity(id) + 0L;
        map.put("num", num);
        return map;
    }


    //5.修改商品信息
    @Override
    @Transactional(propagation = Propagation.REQUIRED, timeout = -1, rollbackFor = Exception.class)
    public Map<String, Object> updateCommodity(Commodity commodity) {
        Map<String, Object> map = new ConcurrentHashMap<>();
        commodity.setCreateDate(new Date());
        Long num = commodityMapper.updateCommodity(commodity) + 0L;
        map.put("commodity", commodity);
        map.put("num", num);
        return map;
    }

    @Override
    public Map<String, Object> updateTest() {
        Map<String, Object> map = new ConcurrentHashMap<>();
        List<Integer> list = new ArrayList<>();
        Commodity commodity;
        for (int i = 1; i <= 112; i++) {
            commodity = commodityMapper.getCommdityById(i + 0L);
            if (commodity != null) {
                Integer num = commodityMapper.updateTest(i);
                if (num > 0) {
                    list.add(num);
                }
            }
        }
        map.put("list", list);
        return map;
    }

    @Override
    public Map<String, Object> commodityList(Map<String,Object> map0) {
//        , String sort, Integer pageNum, Integer pageCount

//        if (name == null) {
//            name = "";
//        }
        Integer pageNum = (Integer)map0.get("pageNum");
        Integer pageCount = (Integer)map0.get("pageCount");
        Map<String, Object> map = new ConcurrentHashMap<>();
        Integer totalCount = commodityMapper.getSum(map0);
        Page<Commodity> page = new Page<>();
        page.init(pageNum, pageCount, totalCount);
        map0.put("rowNum",page.getRowNum());
        List<Commodity> list = commodityMapper.commodityList(map0);
        page.setList(list);
        map.put("page", page);
//        map.put("name", name);
        return map;
    }
}
