package com.yu.util;

import java.util.List;

public class Page<T> {
    private Integer pageNum;
    private Integer pageCount;
    private Integer totalPage;
    private Integer totalCount;
    private Integer rowNum;
    private List<T> list;


    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {

        if(pageNum==null||pageNum<=1){
            this.pageNum=1;
            return;
        }
        this.pageNum = pageNum;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {

        if(pageCount==null||pageCount<=0){
            this.pageCount = 10;
            return;
        }
        this.pageCount = pageCount;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalCount%pageCount==0?totalCount/pageCount:totalCount/pageCount+1;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {

        if(totalCount==null||totalCount<=0){
            this.totalCount = 0;
            return;
        }
        this.totalCount = totalCount;
    }

    public Integer getRowNum() {
        return rowNum;
    }

    public void setRowNum(Integer rowNum) {
        this.rowNum = pageCount*(pageNum-1);
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Page{" +
                "pageNum=" + pageNum +
                ", pageCount=" + pageCount +
                ", totalPage=" + totalPage +
                ", totalCount=" + totalCount +
                ", rowNum=" + rowNum +
                ", list=" + list +
                '}';
    }

    public void init(Integer pageNum,Integer pageCount,Integer totalCount){
        setTotalCount(totalCount);
        setPageCount(pageCount);
        setTotalPage(0);
        setPageNum(pageNum);
        setRowNum(0);
    }
}
