package com.yu.util;

import java.math.BigInteger;
import java.security.MessageDigest;

public class Encoding {

    /************** MD5加密方法***************/
    public static String encoding(String string){
        if(string==null){
            return null;
        }
        BigInteger bigInteger = null;
        try{
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(string.getBytes());
            bigInteger = new BigInteger(1,messageDigest.digest());
        }catch(Exception e){
            e.printStackTrace();
        }
        return bigInteger.toString(16).toUpperCase();
    }

    public static void main(String[] args) {
        System.out.println(encoding("admin123"));
    }
}
