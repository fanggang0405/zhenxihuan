package com.yu.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yu.constant.WX;
import com.yu.mapper.UserMapper;
import com.yu.util.HttpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RestController
@CrossOrigin
public class UserController {

    @Autowired
    UserDetailsService userDetailsService;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    RedisTemplate<String, String> redisTemplate;

    @Autowired
    UserMapper userMapper;

    @GetMapping("/wxlogin")
    @Transactional
    public String findOpenId(String code){

        String url = "https://api.weixin.qq.com/sns/jscode2session?" +
                "appid="+ WX.APPID +
                "&secret="+WX.APPSEC+"" +
                "&js_code=" +"011iJFms0aK6bh1j0Sls0r3Hms0iJFmF"+
                "&grant_type=authorization_code";
        String html = HttpUtil.getHtml(url);
        System.out.println(html);
        JSONObject jsonObject = JSON.parseObject(html);
        System.out.println(jsonObject);

        String openid = jsonObject.getString("openid");
        String session_key = jsonObject.getString("session_key");
        //作为微信用户，如果是第一次访问后台，需要先注册
        //如果该用户已经存在数据库中
        String dbToken = userMapper.is_user_openid_repeat(openid);
        if(dbToken!=null){
            return openid;
        }
        //
        Map map = new HashMap();
        map.put("openid",openid);
        map.put("sessionKey",session_key);
        String password = (new BCryptPasswordEncoder()).encode(openid+"wengchaofeng");
        map.put("password",password);
        userMapper.add_user(map);
        //
        String token = generateToken(openid,password);
        redisTemplate.opsForValue().set(token,openid);

        map = new HashMap();
        map.put("openId",openid);
        map.put("token",token);
        userMapper.update_user_token(map);

        return token;
    }

    private String generateToken(String username, String password) {
        UsernamePasswordAuthenticationToken upToken = new UsernamePasswordAuthenticationToken(username, password);
        final Authentication authentication = authenticationManager.authenticate(upToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        String token = UUID.randomUUID().toString();
        return token;
    }

}
