package com.yu.controller;

import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

@RestController
@CrossOrigin
public class ImageController {

    @RequestMapping("/find_img")
    public void find_img(String fileName, HttpServletResponse response){
        String osName = System.getProperty("os.name");
        ServletOutputStream servletOutputStream = null;

        try {
            String path =
                    (osName.startsWith("Windows")?"E:\\idea_workSpace\\picpath\\":"/upload/")+fileName;
            System.out.println(path);
            BufferedInputStream bufferedInputStream =
                    new BufferedInputStream(new FileInputStream(new File(path)));
            int num ;
            byte[] bytes = new byte[1024];
            servletOutputStream = response.getOutputStream();
            while ((num=bufferedInputStream.read(bytes))!=-1){
                servletOutputStream.write(bytes,0,num);
                System.out.println(bytes);
            }
            servletOutputStream.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(servletOutputStream!=null){
                try {
                    servletOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
