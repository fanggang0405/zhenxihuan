package com.yu.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.sun.deploy.net.HttpResponse;
import com.yu.mapper.CommodityMapper;
import com.yu.pojo.Commodity;
import com.yu.pojo.CommodityType;
import com.yu.service.CommodityService;
import com.yu.util.FileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;


@RestController
@CrossOrigin
@RequestMapping("/commodity")
public class CommodityController {

    @Autowired
    CommodityMapper commodityMapper;

    @Resource
    private CommodityService commodityService;

    //1.getCommdityList----------------------废弃了的方法。。。。。--------
    public String getCommdityList(){
        Map<String,Object> map = commodityService.getCommdityList();
        return JSON.toJSONStringWithDateFormat(map,"yyyy-MM-dd HH:mm:ss", SerializerFeature.DisableCircularReferenceDetect);
    }

    //2.getCommdityById
    @PostMapping(value = "/getCommdityById")
    public String getCommdityById(@RequestBody Map<String,Object> map1 ){
        Integer id = (Integer) map1.get("id");
        System.out.println("----------->"+id);
        Map<String,Object> map = commodityService.getCommdityById(id+0L);
        return JSON.toJSONStringWithDateFormat(map,"yyyy-MM-dd HH:mm:ss", SerializerFeature.DisableCircularReferenceDetect);
    }

    //3.add
    @PostMapping(value = "/addCommodity")
    public String addCommodity(@RequestBody Commodity commodity){
        try {
            System.out.println("----------->"+commodity);
            Map<String, Object> map = commodityService.addCommodity(commodity);
            return JSON.toJSONStringWithDateFormat(map,"yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteDateUseDateFormat);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //4.deleteCommodity
    @PostMapping(value = "/deleteCommodity")
    public String deleteCommodity(@RequestBody Map<String,Object> map1 ){
        try {
            Integer id = (Integer) map1.get("id");
            System.out.println("----------->"+id);
            Map<String, Object> map = commodityService.deleteCommodity(id+0L);
            return JSON.toJSONStringWithDateFormat(map,"yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteDateUseDateFormat);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //5.updateCommodity
    @PostMapping(value = "/updateCommodity")
    public String updateCommodity(@RequestBody Commodity commodity){
        try {
            System.out.println("----------->"+commodity);
            Map<String, Object> map = commodityService.updateCommodity(commodity);
            return JSON.toJSONStringWithDateFormat(map,"yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteDateUseDateFormat);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    //6.updateTest
//    @GetMapping(value = "/updateTest")
    public String updateTest(){
        try {
            Map<String, Object> map = commodityService.updateTest();
            return JSON.toJSONStringWithDateFormat(map,"yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteDateUseDateFormat);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //7.commodityList----------------------分页---------------------------
    @PostMapping("/commodityList")
    public String commodityList(@RequestBody Map<String,Object> map1){
//        String name = (String) map1.get("name");
//        Integer pageNum = (Integer) map1.get("pageNum");
//        Integer pageCount = (Integer) map1.get("pageCount");
//        String sort = (String) map1.get("sort");
//
//        System.out.println("----------name--->"+name+"---page--->"+pageNum+"---limit--->"+pageCount+"---sort--->"+sort);
//        Map<String,Object> map = commodityService.commodityList(name,sort,pageNum,pageCount);
        Map<String,Object> map = commodityService.commodityList(map1);
        return JSON.toJSONStringWithDateFormat(map,"yyyy-MM-dd HH:mm:ss", SerializerFeature.DisableCircularReferenceDetect);
    }

    //8.百度编辑器请求上传图片配置文件（返回配置文件的字符串）
    @GetMapping(value ="/find_upload_config",produces="text/javascript; charset=utf-8")
    public String configUpload(HttpServletRequest request, HttpServletResponse response) throws IOException {

        try {
            String action = (String) request.getParameter("action");
            if(action!=null){
                String callback =  (String) request.getParameter("callback");
                String s = FileUtil.readFileAsString("E:\\config\\ueditorconfig.txt");
                return callback+"("+s+")";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    //9.上传图片
    @PostMapping("/upload")
    public Map handleFileUpload(String token,HttpServletRequest request,@RequestParam("file") MultipartFile file) {
        String oldFileName = file.getOriginalFilename();
        int lastDotIndex = oldFileName.lastIndexOf(".");
        String extName = oldFileName.substring(lastDotIndex);
        String newName = System.currentTimeMillis() +extName;
        String os = System.getProperty("os.name");
        try {
            File excelFile =  new File(
                    (os.startsWith("Windows")?"E:\\idea_workSpace\\picpath\\":"/upload/")+newName);
            System.out.println("-----excelFile------>"+excelFile.getAbsolutePath());
            String url = request.getRequestURL()+"";
            url = url.substring(0,url.lastIndexOf("/commodity"));
            System.out.println(url);

            file.transferTo(excelFile);
            Map map = new HashMap();
            map.put("original",oldFileName);
            map.put("size",file.getSize());
            map.put("state", "SUCCESS");
            map.put("title", newName);
            map.put("type", extName);
            map.put("url", url+"/find_img?fileName="+newName+"&token="+token);
            return map;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    //10.getAllCommodityType
    @PostMapping("/getAllCommodityType")
    public List<CommodityType> getAllCommodityType(){
        return commodityMapper.getAllCommodityType();
    }
}
