package com.yu.controller;

import com.yu.mapper.MerchantMapper;
import com.yu.pojo.Merchant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/merchant")
public class MerchantController {

    @Autowired
    MerchantMapper merchantMapper;

    @PostMapping("/getAll")
    public Map getAll(){
        List<Merchant> list = merchantMapper.getAll();
        Map map = new HashMap();
        map.put("list",list);
        return map;
    }
}
