package com.yu.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class Users implements Serializable {
    private Integer id;
    private Integer openId;
    private String sessionKey;
    private String token;
    private String nickName;
    private String password;
    private Date createDate;
    private String address;
    private Integer gender;
    private Integer roleId;
    private String unionId;

}
