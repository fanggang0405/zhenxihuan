package com.yu.pojo;

import lombok.Data;

@Data
public class PicMerchant {

    private Long id;
    private Long merchantId;
    private String picpath;
}
