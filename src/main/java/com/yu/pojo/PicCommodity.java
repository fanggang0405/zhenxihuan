package com.yu.pojo;

import lombok.Data;

@Data
public class PicCommodity {
    private Integer id;
    private Integer commodityId;
    private String picpath;
}
