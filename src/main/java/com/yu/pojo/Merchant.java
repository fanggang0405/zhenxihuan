package com.yu.pojo;

import lombok.Data;

import java.util.List;

@Data
public class Merchant {

    private Long id;
    private String name;
    private String address;
    private Long status;
    private String password;
    private String telNum;
    private List<PicMerchant> picMerchant;
}
