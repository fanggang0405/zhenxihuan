package com.yu.pojo;

import lombok.Data;

@Data
public class CommodityType {

  private Long id;
  private String name;

}
