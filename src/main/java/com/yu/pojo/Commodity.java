package com.yu.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Data
public class Commodity {

  private Long id;
  private Long merchantId;
  private Long picId;
  private String name;
  private Double newPrice;
  private Double oldPrice;
  private Long type;
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Date createDate;
  private String picpath;
  private Long period;
  private Long status;
  private String description;
  private Long store;

  private List<PicCommodity> picCommodityList;
  private CommodityType commodityType;
  private Merchant merchant;

}
